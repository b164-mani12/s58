import { useState, Fragment, useContext} from 'react';
import {Navbar, Nav, Container} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import UserContext from '../UserContext';

export default function AppNavbar() {
	const { user } = useContext(UserContext);

	/*
		Syntax:
			localStorage.getItem(propertyName)
	*/
	//const [user, setUser] = useState(localStorage.getItem("email"));
	//console.log(user)

	return(
		<Navbar bg="green" expand="lg" variant = "light"  >
		  <Container>
		    <Navbar.Brand as={Link} to="/" >REJ Trading</Navbar.Brand>
		    <Navbar.Toggle aria-controls="basic-navbar-nav" />
		    <Navbar.Collapse id="basic-navbar-nav">
		      <Nav className="me-auto">
		       {/* <Nav.Link as={Link} to="/">Home</Nav.Link>*/}
		       {/* <Nav.Link as={Link} to="/items">Items</Nav.Link>*/}

		        { (user.id !== null) ?
		        	<Nav.Link as={Link} to="/logout"> Logout</Nav.Link>
		        	:
		        	<Fragment>
		        		<Nav.Link as={Link} to="/register">Register</Nav.Link>
		        		<Nav.Link as={Link} to="/login">Login</Nav.Link>
		        	</Fragment>
		        }

		      </Nav>
		    </Navbar.Collapse>
		  </Container>
		</Navbar>
		)
}	